package name.engmark.insecureLinksHighlighter;

import org.apache.commons.io.FileUtils;
import org.json.JSONObject;
import org.junit.After;
import org.junit.Before;
import org.junit.Rule;
import org.junit.rules.TestWatcher;
import org.junit.runner.Description;
import org.openqa.selenium.By;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.TimeoutException;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.firefox.FirefoxOptions;
import org.openqa.selenium.firefox.FirefoxProfile;
import org.openqa.selenium.support.ui.ExpectedCondition;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.time.Duration;
import java.util.Arrays;
import java.util.Objects;
import java.util.UUID;

import static com.google.common.io.Resources.getResource;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

public class TestCase {
    private static final Path CURRENT_PATH = Paths.get(System.getProperty("user.dir"));
    private static final Duration LOCAL_PAGE_TIMEOUT = Duration.ofMillis(5_000);
    private static final UUID ADD_ON_UUID = UUID.randomUUID();
    private static WebDriver driver;
    private static WebDriverWait localWaiter;

    @Rule
    public TestWatcher watcher = new TestWatcher() {
        @Override
        protected void failed(final Throwable testException, final Description description) {
            final String screenshotFilename = String.format("%s.png", description.getMethodName());
            final String screenshotPath = Paths.get(CURRENT_PATH.toString(), "build", screenshotFilename).toString();
            final File screenshotFile = new File(screenshotPath);
            final File screenshot = ((TakesScreenshot) driver).getScreenshotAs(OutputType.FILE);
            try {
                FileUtils.copyFile(screenshot, screenshotFile);
            } catch (IOException exception) {
                throw new RuntimeException(exception);
            }
            super.failed(testException, description);
        }
    };

    @Before
    public void startDriver() throws IOException {
        final FirefoxProfile profile = new FirefoxProfile();
        profile.setPreference("xpinstall.signatures.required", false);
        enableOptionsPageAccess(profile);
        final File extension = new File(Paths.get("insecure-links-highlighter.xpi").toString());
        profile.addExtension(extension);

        final FirefoxOptions options = new FirefoxOptions();
        options.setBinary(getFirefoxExecutablePath());
        options.addArguments("-headless");
        options.setProfile(profile);

        driver = new FirefoxDriver(options);
        localWaiter = new WebDriverWait(driver, LOCAL_PAGE_TIMEOUT);
    }

    private static String getFirefoxExecutablePath() {
        // Work around https://github.com/mozilla/geckodriver/issues/2178#issuecomment-2081891620
        String executableName = "firefox-developer-edition";
        String pathDirs = System.getenv("PATH");
        for (String pathDir : pathDirs.split(File.pathSeparator)) {
            File file = new File(pathDir, executableName);
            if (file.isFile() && file.canExecute()) {
                return file.getAbsolutePath();
            }
        }
        throw new AssertionError(String.format("Executable %s was not found on PATH %s", executableName, pathDirs));
    }

    private static void enableOptionsPageAccess(final FirefoxProfile profile) throws IOException {
        profile.setPreference("extensions.webextensions.uuids", new JSONObject().put(getAddOnId(), ADD_ON_UUID).toString());
    }

    private static String getAddOnId() throws IOException {
        return getManifest().getJSONObject("applications").getJSONObject("gecko").getString("id");
    }

    private static JSONObject getManifest() throws IOException {
        return new JSONObject(new String(Files.readAllBytes(Paths.get("manifest.json"))));
    }

    @After
    public void stopDriver() {
        if (driver != null) {
            driver.quit();
        }
    }

    protected static void assertLinkIsHighlighted(final String linkText) {
        assertTrue(linkIsHighlighted(linkText));
    }

    protected static void assertLinkIsNotHighlighted(final String linkText) {
        assertFalse(linkIsHighlighted(linkText));
    }

    protected static boolean linkIsHighlighted(final String linkText) {
        try {
            return new WebDriverWait(driver, Duration.ofSeconds(2)).until((ExpectedCondition<Boolean>) input -> {
                final WebElement link = driver.findElement(By.linkText(linkText));
                final String[] classes = link.getAttribute("class").split(" ");
                return Arrays.asList(classes).contains("insecure-links-highlighter-highlighted");
            });
        } catch (TimeoutException exception) {
            return false;
        }
    }

    protected static void getIndex() {
        String pagePath = Objects.requireNonNull(getResource("index.html")).getPath();
        driver.get("file://" + pagePath);
    }

    protected static void getOptionsPage() {
        driver.get(String.format("moz-extension://%s/options.html", ADD_ON_UUID));
    }

    protected static WebDriver getDriver() {
        return driver;
    }

    protected static WebDriverWait getLocalWaiter() {
        return localWaiter;
    }
}
