/*
Copyright 2018 Victor Engmark

This file is part of Insecure Links Highlighter.

This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.

This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more details.

You should have received a copy of the GNU General Public License along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

/* global browser, defaultOptions */

function saveOptions(event) {
    event.preventDefault();

    const borderColorElement = document.querySelector("#borderColor");
    const borderColor = borderColorElement.value;
    const borderColorMessageElementId = "borderColorMessage";
    const messageElement = document.querySelector(
        "#" + borderColorMessageElementId,
    );

    if (messageElement !== null) {
        messageElement.remove();
    }
    if (!isValidColorString(borderColor)) {
        const messageElement = document.createElement("p");
        const message = browser.i18n.getMessage(
            "invalidBorderColorMessage",
            borderColor,
        );
        const messageTextNode = document.createTextNode(message);
        messageElement.id = borderColorMessageElementId;
        messageElement.classList.add("error");
        messageElement.appendChild(messageTextNode);
        borderColorElement.parentElement.parentElement.insertAdjacentElement(
            "afterend",
            messageElement,
        );
        return;
    }

    browser.storage.local.set({
        borderColor,
        elementsWithEventHandlersAreInsecure: document.querySelector(
            "#elementsWithEventHandlersAreInsecure",
        ).checked,
    });
}

function restoreOptions() {
    browser.storage.local.get(defaultOptions).then(setOptions);

    function setOptions(options) {
        document.querySelector("#borderColor").value = options.borderColor;
        if (options.elementsWithEventHandlersAreInsecure) {
            document
                .querySelector("#elementsWithEventHandlersAreInsecure")
                .setAttribute("checked", "checked");
        }
    }
}

function setDefaultsAndRestoreOptions() {
    browser.storage.local.set(defaultOptions);
    restoreOptions();
}

function isValidColorString(color) {
    if (["", "inherit", "transparent"].includes(color)) {
        return false;
    }

    const imageElement = document.createElement("img");
    imageElement.style.color = "rgb(0, 0, 0)";
    imageElement.style.color = color;
    if (imageElement.style.color !== "rgb(0, 0, 0)") {
        return true;
    }
    imageElement.style.color = "rgb(255, 255, 255)";
    imageElement.style.color = color;
    return imageElement.style.color !== "rgb(255, 255, 255)";
}

document.addEventListener("DOMContentLoaded", restoreOptions);
document.querySelector("#borderColor").addEventListener("change", saveOptions);
document
    .querySelector("#elementsWithEventHandlersAreInsecure")
    .addEventListener("change", saveOptions);
document
    .querySelector("form")
    .addEventListener("reset", setDefaultsAndRestoreOptions);
