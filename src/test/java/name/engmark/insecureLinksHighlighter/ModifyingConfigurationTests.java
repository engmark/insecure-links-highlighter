package name.engmark.insecureLinksHighlighter;

import org.junit.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.support.ui.ExpectedConditions;

public class ModifyingConfigurationTests extends TestCase {
    @Test
    public void shouldHighlightUrlWithOnmousedownHandlerWhenConfigured() {
        getOptionsPage();
        getLocalWaiter().until(ExpectedConditions.visibilityOfElementLocated(By.id("elementsWithEventHandlersAreInsecure"))).click();
        getLocalWaiter().until(Conditions.elementsWithEventHandlersAreInsecureIsSet());

        // Verify functionality
        getIndex();
        assertLinkIsHighlighted("modify on mouse down");
    }
}
