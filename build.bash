#!/usr/bin/env bash

set -o errexit -o noclobber -o nounset -o pipefail
shopt -s failglob inherit_errexit

name='insecure-links-highlighter'

for dimension in 48 96; do
    magick icons/icon.svg -resize "${dimension}x${dimension}" icons/${dimension}.png
done

oxipng --opt=max icons/*.png

xpi_files=(
    _locales
    _locales/*
    _locales/*/messages.json
    defaultOptions.js
    dom.js
    highlight.js
    icons
    icons/*.png
    manifest.json
    options.html
    options.js
    url.js
)
touch -t 198001010000 "${xpi_files[@]}"
chmod go= "${xpi_files[@]}"

# Work around zip not overwriting output file by default
xpi_file="${name}.xpi"
if [[ -e "$xpi_file" ]]; then
    rm "$xpi_file"
fi

zip -9X "${name}.xpi" "${xpi_files[@]}"

gradle --configure-on-demand --info --rerun-tasks check

npm ci
npx nyc mocha test/unit
