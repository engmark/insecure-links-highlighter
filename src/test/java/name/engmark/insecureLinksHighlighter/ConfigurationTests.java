package name.engmark.insecureLinksHighlighter;

import org.junit.Before;
import org.junit.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.support.ui.ExpectedConditions;

public class ConfigurationTests extends TestCase {
    @Before
    public void loadPage() {
        getOptionsPage();
    }

    @Test
    public void shouldUseRedBorderColorByDefault() {
        getLocalWaiter().until(ExpectedConditions.attributeToBe(By.id("borderColor"), "value", "red"));
    }

    @Test
    public void shouldNotTreatElementsWithEventHandlersAsInsecureByDefault() {
        getLocalWaiter().until(ExpectedConditions.elementSelectionStateToBe(By.id("elementsWithEventHandlersAreInsecure"), false));
    }
}
