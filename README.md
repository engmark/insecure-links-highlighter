# Insecure Links Highlighter

Easily notice insecure links in Firefox by their bright red border:

[![Screenshot of insecure links](screenshots/links.png "Screenshot of insecure link")](screenshots/links.html)

Highlights insecure links including plaintext HTTP and
[dynamic links](#dynamic-links) (optional).

## Download

[Firefox add-on](https://addons.mozilla.org/firefox/addon/insecure-links-highlighter/)

## Build/test/release

Prerequisites: [Nix](https://nixos.org/download.html)

1. Update the version in [manifest.json](manifest.json)
1. Commit changes
1. Make sure the repository is clean, that is, `git status --porcelain` prints
   nothing
1. Remove any previous build results with `git clean --exclude=.idea -fdx`
1. Make sure the project builds from scratch with
   `nix-shell --pure --run ./build.bash`
1. Tag the release with `git tag $(jq -r .version < manifest.json) -m "Release"`
1. Push the changes with `git push --atomic --follow-tags`
1. [Upload](https://addons.mozilla.org/en-US/developers/addon/insecure-links-highlighter/versions/submit/)
   the new .xpi

## Dynamic links

These are links which execute code when certain things happen, such as clicking
a link. This does have legitimate and safe uses, but even the world's biggest
search engine uses it sneakily: when you move the mouse over a link anywhere on
the web, Firefox displays the URL it expects to go to when that link is clicked.
But dynamic links often go to a different page first, for example to keep track
of which links you have clicked. Seeing the wrong link in the browser therefore
gives a false sense of privacy and security.

## [License](LICENSE)

GNU GPL version 3 or later.

## Acknowledgements

Special thanks to [Catalyst](https://catalyst.net.nz/) for letting me work on
this between projects.
